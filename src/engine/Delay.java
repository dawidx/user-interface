package engine;

public class Delay extends Effect {

	public static int ID = 4;

    private int _Time = 200;
    private int _Strength = 30;
    
	public Delay() {
		super(4);
	}

	@Override
	public byte[] getUpdateSequence() {
		byte[] sequence = new byte[12];
        sequence[0] = (byte) this.EffectID;
        sequence[1] = (byte) this.Volume;
        sequence[2] = (byte) this._Time;
        sequence[3] = (byte) this._Strength;
        sequence[4] = (byte) 0;
        sequence[5] = (byte) 0;
        sequence[6] = (byte) 0;
        sequence[7] = (byte) 0;
        sequence[8] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[10] = (byte) 0;
        sequence[11] = (byte) 0;
        return sequence;
	}

    public void setStrength(Integer strength) {
        this._Strength = strength;
    }
    
    public int getStrength() {
    	return this._Strength;
    }
    
    public void setDelay(Integer time) {
        this._Time = time;
    }
    
    public int getDelay() {
    	return this._Time;
    }
	
}
