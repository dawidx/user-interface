/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

/**
 *
 * @author Dawid
 */
public class Chorus extends Effect {
    
    public static int ID = 2;
    
    private int _Time = 80;
    private int _Guitar1 = 100;
    private int _Guitar2 = 100;
    private int _Variant = 2;
    
    public Chorus() {
        super(2);
    }

    @Override
    public byte[] getUpdateSequence() {
        byte[] sequence = new byte[12];
        sequence[0] = (byte) this.EffectID;
        sequence[1] = (byte) this.Volume;
        sequence[2] = (byte) this._Time;
        sequence[3] = (byte) this._Guitar1;
        sequence[4] = (byte) this._Guitar2;
        sequence[5] = (byte) this._Variant;
        sequence[6] = (byte) 0;
        sequence[7] = (byte) 0;
        sequence[8] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[10] = (byte) 0;
        sequence[11] = (byte) 0;
        return sequence;
    }
    
    public void setTime(Integer time) {
        this._Time = time;
    }
    
    public int getTime() {
    	return this._Time;
    }
    
    public void setGuitar1(Integer volume) {
        this._Guitar1 = volume;
    }
    
    public int getGuitar1() {
    	return this._Guitar1;
    }
    
    public void setGuitar2(int volume) {
        this._Guitar2 = volume;
    }
    
    public int getGuitar2() {
    	return this._Guitar2;
    }
    
    public void setVariant(int variant) {
    	this._Variant = variant;
    }
    
    public int getVariant() {
    	return this._Variant;
    }
}
