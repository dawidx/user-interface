package engine;

public class HighPass extends Effect {

	private int _Breakpoint = 5;
	
	public static int ID = 6;

	public HighPass() {
		super(6);
	}
	
	@Override
	public byte[] getUpdateSequence() {
		System.out.println("getUpdatedSequence 1");
        byte[] sequence = new byte[12];
        System.out.println("getUpdatedSequence 2");
        sequence[0] = (byte) this.EffectID;
        System.out.println("getUpdatedSequence 3");
        sequence[1] = (byte) this.Volume;
        System.out.println("getUpdatedSequence 4");
        sequence[2] = (byte) this._Breakpoint;
        sequence[3] = (byte) 0;
        sequence[4] = (byte) 0;
        sequence[5] = (byte) 0;
        sequence[6] = (byte) 0;
        sequence[7] = (byte) 0;
        sequence[8] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[10] = (byte) 0;
        sequence[11] = (byte) 0;
        System.out.println("getUpdatedSequence 5");
        return sequence;
	}

	
	public void setBreakpoint(int Breakpoint) {
		this._Breakpoint = Breakpoint / 100;
	}
	
	public int getBreakpoint() {
		return this._Breakpoint * 100;
	}
}
