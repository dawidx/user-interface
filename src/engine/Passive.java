/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

/**
 *
 * @author Dawid
 */
public class Passive extends Effect {
    
    public static int ID = 0;
    
    public Passive() {
        super(0);
    }

    @Override
    public byte[] getUpdateSequence() {
        byte[] sequence = new byte[12];
        sequence[0] = (byte) this.EffectID;
        sequence[1] = (byte) this.Volume;
        sequence[2] = (byte) 0;
        sequence[3] = (byte) 0;
        sequence[4] = (byte) 0;
        sequence[5] = (byte) 0;
        sequence[6] = (byte) 0;
        sequence[7] = (byte) 0;
        sequence[8] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[10] = (byte) 0;
        sequence[11] = (byte) 0;
        return sequence;
    }
    
}
