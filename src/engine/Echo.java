/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

/**
 *
 * @author Dawid
 */
public class Echo extends Effect {
    
    public static int ID = 3;
    
    private int Time = 200;
    private int Strength = 30;
    
    public Echo() {
        super(3);
    }

    @Override
    public byte[] getUpdateSequence() {
        byte[] sequence = new byte[12];
        sequence[0] = (byte) this.EffectID;
        sequence[1] = (byte) this.Volume;
        sequence[2] = (byte) this.Time;
        sequence[3] = (byte) this.Strength;
        sequence[4] = (byte) 0;
        sequence[5] = (byte) 0;
        sequence[6] = (byte) 0;
        sequence[7] = (byte) 0;
        sequence[8] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[9] = (byte) 0;
        sequence[10] = (byte) 0;
        sequence[11] = (byte) 0;
        return sequence;
    }
    
    public void setStrength(Integer strength) {
        this.Strength = strength;
    }
    
    public int getStrength() {
    	return this.Strength;
    }
    
    public void setDelay(Integer time) {
        this.Time = time;
    }
    
    public int getDelay() {
    	return this.Time;
    }
    
}
