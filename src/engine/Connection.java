package engine;

import java.io.*;

/**
 * @author Dawid Ochal
 * @description Keeps the connection between MBed and computer. The class is a singleton that also keeps track
 * of the current settings. 
 */
public class Connection {
	
	/**
	 * @description Holds the instance of the class
	 */
    private static Connection _Instance = null;
    
    /**
     * @description Holds the current settings of the MBed
     */
    private Effect[][] settings; 

    /**
     * @description Private constructor of the Connection class that can only be instanciated via Connection.getInstance()
     */
    private Connection() {
    	//Creating the array of effects (parallel, serial)
    	this.settings = new Effect[4][4];
    	//The initial settings are just like the ones on the MBed Board
    	//@see Reset_Filters();
    	//For each channel 
    	for (int i = 0; i < 4; i++) {
    		//and effect in series in that channel
    		for(int j = 0; j < 4; j++) {
    			//create a passive effect with volume 0
    			Passive p = new Passive();
    			p.setVolume(0);
    			this.settings[i][j] = p;
    		}
    	}
    	//In first effect in the first channel, create a passive filter with volume 100;
    	Passive p = new Passive();
    	p.setVolume(100);
    	this.settings[0][0] = p;
    }

    /**
     * @description The methods sets and sends the set properties of a given effect in a given channel.
     * @param channel the channel to which the effect applies
     * @param number the number of the effect in the channel (0-3)
     * @param effect the effect with all the properties
     */
    public void setParams(int channel, int number, Effect effect) {
    	//Creating array of 20 bytes
        byte[] data = new byte[20];
        //First byte indicates that we are editing an effect
        data[0] = (byte) 0;
        //Second byte indicates the channel that we want to edit
        data[1] = (byte) channel;
        //Third byte indicates the number of the effect in the channel 
        data[2] = (byte) number;
        //Below we add the properties of the effect set by the user
        byte[] params = effect.getUpdateSequence();
        //for each possible parameter in the effect (there can be up to 10 parameters) 
        for (int i = 0; i < params.length; i++) {
        	//add the element to the bytes that will be send
            data[i+3] += params[i];
        }
        try {
        	//Attempt to send the data
            this._sendData(data);
        }catch(IOException e) {
        	//Should anything go wrong we get a debug message
            System.out.println(e.getMessage());
        }
    }

    /**
     * @description Private method that sends the data to the MBed using (in version 3) FileOutputStream
     * @version 3
     * @param data the bytes to be send.
     * @throws IOException whenever the data transfer is in some way unsuccessful
     */
    private void _sendData(byte[] data) throws IOException {
    	// for each byte in the array
    	for (int i = 0; i < data.length; i++) {
    		//creating an array of bits that consists only of one byte
	    	byte[] ll = {(byte) data[i]};
	    	//Creating an output stream to send the data
	    	FileOutputStream fos = new FileOutputStream("/dev/ttyACM0");
	    	//
	        fos.write(ll, 0, ll.length);
	    	fos.flush();
	    	fos.close();
	    	try {
	    		//This is crucial. We can't send too many bits at the same time. So we stop for 50ms after each byte.
	    		//So given array of 20 bytes it takes 1s (20 * 50ms = 1000ms) to update
				Thread.sleep(50);
			} catch (InterruptedException e) {
				//For debugging purposes
				e.printStackTrace();
			}
    	}
    }
    
    /**
     * @description The method returns the effect in a given channel and effect number. It can return either Echo, Delay, Distortion, Chorus or Passive
     * @param Channel The channel 
     * @param Effect The number of the effect in series in the channel
     * @return Effect with the current properties 
     */
    public Effect getEffect(int Channel, int Effect) {
    	return settings[Channel][Effect];
    }
    
    /**
     * @description The method sets the master volume
     * @param volume the value can be adjusted using a 
     * @return True if successful, false otherwise
     */
    public boolean setVolume(int volume) {
        System.out.println("Setting Volume");
        //Creating the array of 20 bits
        byte[] data = new byte[20];
        //Indicating that we are about to modify the master volume
        data[0] = (byte) 1;
        //Setting the volume
        data[1] = (byte) volume;
        //All the rest don't matter as they can be empty bits
        try {
        	//Attempt to send the data
        	this._sendData(data);
        	return true;
        } catch(Exception e) {
        	return false;
        }
    }
    
    /**
     * @description The class that returns the instance of the class. If instance is not yet created, it will be at first call
     * @return Instance of the Connection class
     */
    public static Connection getInstance() {
    	//If instance not created
        if (_Instance == null) {
        	//Create it
            _Instance = new Connection();
        }
        //return instance
        return _Instance;
    }

}