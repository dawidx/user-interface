/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

/**
 *
 * @author Dawid
 */
public abstract class Effect {
    
    protected int Volume, EffectID;
    
    protected Effect(int EffectID) {
        this.EffectID = EffectID;
    }
    
    public int getID() {
        return this.EffectID;
    }
    
    public int getVolume() {
        return this.Volume;
    }
    
    public void setVolume(int Volume) {
    	this.Volume = Volume;
    }
    
    
    
    /**
     * @desc protocol for the data sequence follows:
     * 1. Effect ID
     * 2. Volume
     * 3. Rest of parameters (10 max)
     * @return 
     */
    public abstract byte[] getUpdateSequence();
    
    public static Effect getInstanceFromID(int ID) {
    	if(ID == Passive.ID) {
    		return new Passive();
    	} else if(ID == Distortion.ID) {
    		return new Distortion();
    	} else if(ID == Chorus.ID) {
    		return new Chorus();
    	} else if(ID == Echo.ID) {
    		return new Echo();
    	} else if(ID == Delay.ID) {
    		return new Delay();
    	} else if(ID == HighPass.ID) {
    		return new HighPass();
    	} else if(ID == LowPass.ID) {
    		return new LowPass();
    	} else {
    		return new Passive();
    	}
    }
    
}
