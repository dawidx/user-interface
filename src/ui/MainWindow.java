package ui;

import javax.swing.*;
import java.awt.*;

/**
 * @description The main window that is displayed when the program starts.
 * The window is divided into two pannels:
 * <ol>
 * <li>Buttons (corresponding to appropriate Channel and Effect)</li>
 * <li>Volume Control</li>
 * </ol>
 * @author Dawid Ochal
 */
public class MainWindow extends JFrame {
    
	/**
	 * @ID using for serializing the class
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @description Contains buttons to edit the appropriate Channel and Effect in it
	 */
    private final JPanel _Channels;
    
    /**
     * @description Contains JSlider to modify the master volume
     */
    private final JPanel _Volume;
    
    public MainWindow() {
    	//Instantiating the parent class
        super("Guitar Processor Controller");
        //Instantiating both pannels
        this._Channels = new JPanel();
        this._Volume = new JPanel();
        
        //Indicating that we want a FlowLayout so that buttons go
        // Ch: 0 No: 1 --- Ch: 0 No: 2 --- Ch: 0 No: 3 --- Ch: 0 No: 4
        // Ch: 1 No: 1 --- Ch: 1 No: 2 --- Ch: 1 No: 3 --- Ch: 1 No: 4
        // Ch: 2 No: 1 --- Ch: 2 No: 2 --- Ch: 2 No: 3 --- Ch: 3 No: 4
        // ETC
        // Window size will determine the borders
        this._Channels.setLayout(new FlowLayout());
        //Java windows by default use BorderLayout
        //@see http://docs.oracle.com/javase/7/docs/api/java/awt/BorderLayout.html
        this.add(this._Channels, BorderLayout.CENTER);
        this.add(this._Volume, BorderLayout.SOUTH);
        
        //We close the application when X button is pressed
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Setting the preferred size of the window which is exactly the same
        //as the minimum and maximum to keep the buttons in the right position
        this.setPreferredSize(new Dimension(420, 250));
        this.setMinimumSize(new Dimension(380, 250));
        this.setMaximumSize(new Dimension(380, 250));
        this.setResizable(false);
        
        //Here we call a method to create objects 
        this._Initialise();
        //Display the window
        this.pack();
    }
    /**
     * @description the method used to fill the panels 
     */
    private void _Initialise() {
    	//For each effect in each channel create a button 
        for(int channel = 0; channel < 4; channel++) {
            for(int effect = 0; effect < 4; effect++) {
                this._Channels.add(new EffectButton(channel, effect));
            }
        }
        
        //Add volume master slider
        this._Volume.add(new JLabel("Volume"), BorderLayout.NORTH);
        this._Volume.add(new MasterVolumeSlider(), BorderLayout.SOUTH);
    }
    
}
