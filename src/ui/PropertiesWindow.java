/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import engine.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.awt.*;

/**
 * @description A window that is used to configure the most
 * @author Dawid Ochal
 */
public class PropertiesWindow extends JFrame implements ActionListener {

    /**
	 * @description Default ID used for serialization
	 */
	private static final long serialVersionUID = 1L;

	private int _Channel, _Effect;

    private JPanel _Panel;
    private JComboBox<String> _AvailableEffects;
    private JPanel _EffectPanel;
    private JButton _OKButton, _CancelButton;
    private JSlider _Volume;
    
    
    
    //Specific for each effect
    protected JSlider _Distortion_Threashold, _Chorus_Time, _Chorus_Volume1, _Chorus_Volume2, _Chorus_Variant, _Echo_Strength, _Echo_Time, _Delay_Time, _Delay_Strength, _LowPass_Breakpoint, _HighPass_Breakpoint;
    

    public PropertiesWindow(Integer Channel, Integer Effect) {
        super("Properties Channel: " + Channel.toString() + " Effect: " + Effect.toString());
        this._Channel = Channel;
        this._Effect = Effect;

        this.Initialise();
        this.setPreferredSize(new Dimension(600, 300));
        this.pack();
    }

    private void Initialise() {
        this._Panel = new JPanel(new BorderLayout());
        this._EffectPanel = new JPanel(new BorderLayout());
        this._EffectPanel.setPreferredSize(new Dimension(400, 400));
        //Defining the combo box
        this._AvailableEffects = new JComboBox<String>(this._getEffects());
        //Selecting the position for the currently selected effect
        this._AvailableEffects.setSelectedIndex(this._getCurrentEffect().getID());
        //Adding the action listener. should one change the effect
        //the effect properties panel will change
        this._AvailableEffects.addActionListener(this);

        //Adding mini-panels to format each element
        JPanel vs = new JPanel(new BorderLayout());
        JPanel vP = new JPanel(new BorderLayout());
        vP.add(new JLabel("Volume:"), BorderLayout.NORTH);
        this._Volume = new JSlider();
        this._Volume.setMinimum(0);
        this._Volume.setMaximum(100);
        vP.add(this._Volume, BorderLayout.SOUTH);
        vs.add(this._AvailableEffects, BorderLayout.NORTH);
        vs.add(vP, BorderLayout.SOUTH);
        this._Panel.add(vs, BorderLayout.NORTH);
        this._Panel.add(this._EffectPanel, BorderLayout.CENTER);
        this._LoadEffectPanel(this._getCurrentEffect());

        JPanel f = new JPanel(new BorderLayout());
        this._OKButton = new JButton("OK");
        this._OKButton.addActionListener(this);
        f.add(this._OKButton);
        this._CancelButton = new JButton("Cancel");
        this._CancelButton.addActionListener(this);
        f.add(this._CancelButton);
        this._Panel.add(f, BorderLayout.SOUTH);
        this.add(this._Panel);
    }

    private void _LoadEffectPanel(Effect effect) {
    	//Firstly, we clear the panel
        this._EffectPanel.removeAll();
        this._Volume.setValue(effect.getVolume());
        //Then we load appropriate set of controls
        if (effect instanceof Passive) {
            //passive
            this._EffectPanel.add(this._loadPassiveEffect((Passive)effect));
        } else if (effect instanceof Distortion) {
            //distortion
            this._EffectPanel.add(this._loadDistortionEffect((Distortion)effect));
        } else if (effect instanceof Chorus) {
        	//chorus
            this._EffectPanel.add(this._loadChorusEffect((Chorus)effect));
        } else if (effect instanceof Echo) {
            //echo
            this._EffectPanel.add(this._loadEchoEffect((Echo)effect));
        } else if (effect instanceof Delay) {
        	//delay
        	this._EffectPanel.add(this._loadDelayEffect((Delay)effect));
        }
        else if (effect instanceof HighPass) {
        	//delay
        	this._EffectPanel.add(this._loadHighPassEffect((HighPass)effect));
        }
        else if (effect instanceof LowPass) {
        	//delay
        	this._EffectPanel.add(this._loadLowPassEffect((LowPass)effect));
        }
        //Then, once the new controls have been added
        //repaint the effectPanel
        this._EffectPanel.invalidate();
        this._EffectPanel.validate(); 
        this._EffectPanel.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == this._OKButton) {
            engine.Connection.getInstance();
            Effect ss = this._setParams(this._AvailableEffects.getSelectedIndex());
            Connection.getInstance().setParams(this._Channel, this._Effect, ss);
        } else if (ae.getSource() == this._AvailableEffects) {
            JComboBox<String> cb = (JComboBox<String>) ae.getSource();
            this._LoadEffectPanel(Effect.getInstanceFromID(cb.getSelectedIndex()));
        } else if(ae.getSource() == this._CancelButton) {
            this.dispose();
        }
    }

    private Effect _setParams(int effectID) {
        if (effectID == Passive.ID) {
                Passive p = new Passive();
                p.setVolume(this._Volume.getValue());
                return p;
            } else if (effectID == Distortion.ID) {
                Distortion d = new Distortion();
                d.setVolume(this._Volume.getValue());
                d.setThreshold(this._Distortion_Threashold.getValue());
                return d;
            } else if (effectID == Chorus.ID) {
                Chorus c = new Chorus();
                c.setGuitar1(this._Chorus_Volume1.getValue());
                c.setGuitar2(this._Chorus_Volume2.getValue());
                c.setTime(this._Chorus_Time.getValue());
                c.setVariant(this._Chorus_Variant.getValue());
                return c;
            } else if (effectID == Echo.ID) {
                Echo e = new Echo();
                e.setDelay(this._Echo_Time.getValue());
                e.setStrength(this._Echo_Strength.getValue());
                return e;
            } else if (effectID == Delay.ID) {
            	Delay d = new Delay();
            	d.setDelay(this._Delay_Time.getValue());
            	d.setStrength(this._Delay_Strength.getValue());
            	return d;
            } else if (effectID == LowPass.ID) {
            	LowPass l = new LowPass();
            	l.setBreakpoint(this._LowPass_Breakpoint.getValue());
            	return l;
            } else if (effectID == HighPass.ID) {
            	HighPass h = new HighPass();
            	h.setBreakpoint(this._HighPass_Breakpoint.getValue());
            	return h;
            } 
            else {
                return new Passive();
            }
    }
    
    private JPanel _loadPassiveEffect(Passive effect) {
        return new JPanel(new BorderLayout());
    }

    private JPanel _loadDistortionEffect(Distortion effect) {
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(300, 300));
        p.add(new JLabel("Threshold:"), BorderLayout.NORTH);
        this._Distortion_Threashold = new JSlider();
        this._Distortion_Threashold.setMaximum(30);
        this._Distortion_Threashold.setMinimum(1);
        this._Distortion_Threashold.setValue(effect.getThreshold());
        p.add(this._Distortion_Threashold, BorderLayout.SOUTH);
        return p;
    }
    
    private JPanel _loadHighPassEffect(HighPass effect) {
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(300, 300));
        p.add(new JLabel("Breakpoint High:"), BorderLayout.NORTH);
        //Lack of Logarithmic scale on the JSliders
        this._HighPass_Breakpoint = new JSlider();
        this._HighPass_Breakpoint.setMaximum(22000);
        this._HighPass_Breakpoint.setMinimum(100);
        this._HighPass_Breakpoint.setMajorTickSpacing(100);
        this._HighPass_Breakpoint.setValue(effect.getBreakpoint());
        p.add(this._HighPass_Breakpoint, BorderLayout.SOUTH);
        return p;
    }
    private JPanel _loadLowPassEffect(LowPass effect) {
    	JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(300, 300));
        p.add(new JLabel("Breakpoint Low:"), BorderLayout.NORTH);
        //Lack of Logarithmic scale on the JSliders
        this._LowPass_Breakpoint = new JSlider();
        this._LowPass_Breakpoint.setMaximum(22000);
        this._LowPass_Breakpoint.setMinimum(100);
        this._LowPass_Breakpoint.setMajorTickSpacing(100);
        this._LowPass_Breakpoint.setValue(effect.getBreakpoint());
        p.add(this._LowPass_Breakpoint, BorderLayout.SOUTH);
        return p;
    }
    
    private JPanel _loadChorusEffect(Chorus effect) {
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(400, 400));
        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(new JLabel("Volume of Guitar 1: "), BorderLayout.NORTH);
        this._Chorus_Volume1 = new JSlider();
        this._Chorus_Volume1.setMaximum(100);
        this._Chorus_Volume1.setMinimum(0);
        this._Chorus_Volume1.setValue(effect.getGuitar1());
        p2.add(this._Chorus_Volume1, BorderLayout.SOUTH);
        p.add(p2, BorderLayout.NORTH);
        JPanel p3 = new JPanel(new BorderLayout());
        p3.add(new JLabel("Volume of Guitar 2: "), BorderLayout.NORTH);
        this._Chorus_Volume2 = new JSlider();
        this._Chorus_Volume2.setMaximum(100);
        this._Chorus_Volume2.setMinimum(0);
        this._Chorus_Volume2.setValue(effect.getGuitar2());
        p3.add( this._Chorus_Volume2);
        p.add(p3, BorderLayout.CENTER);
        JPanel p6 = new JPanel(new BorderLayout());
        JPanel p4 = new JPanel(new BorderLayout());
        p4.add(new JLabel("Delay (ms): "), BorderLayout.NORTH);
        this._Chorus_Time = new JSlider();
        this._Chorus_Time.setMaximum(80);
        this._Chorus_Time.setMinimum(50);
        this._Chorus_Time.setValue(effect.getTime());
        p4.add(this._Chorus_Time, BorderLayout.SOUTH);
        JPanel p5 = new JPanel(new BorderLayout());
        p5.add(new JLabel("Time Variant: "), BorderLayout.NORTH);
        this._Chorus_Variant = new JSlider();
        this._Chorus_Variant.setMaximum(10);
        this._Chorus_Variant.setMinimum(1);
        this._Chorus_Variant.setValue(effect.getVariant());
        p5.add(this._Chorus_Variant, BorderLayout.SOUTH);
        p6.add(p4, BorderLayout.NORTH);
        p6.add(p5, BorderLayout.SOUTH);
        p.add(p6, BorderLayout.SOUTH);
        return p;
    }
    
    private JPanel _loadEchoEffect(Echo effect) {
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(400, 400));
        //
        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(new JLabel("Strength:"), BorderLayout.NORTH);
        this._Echo_Strength = new JSlider();
        this._Echo_Strength.setMaximum(100);
        this._Echo_Strength.setMinimum(0);
        p1.add(this._Echo_Strength, BorderLayout.SOUTH);
        p.add(p1, BorderLayout.NORTH);
        //
        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(new JLabel("Time (ms):"), BorderLayout.NORTH);
        this._Echo_Time = new JSlider();
        this._Echo_Time.setMaximum(50);
        this._Echo_Time.setMinimum(0);
        p2.add(this._Echo_Time, BorderLayout.SOUTH);
        p.add(p2, BorderLayout.SOUTH);
        return p;
    }
    
    private JPanel _loadDelayEffect(Delay effect) {
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(400, 400));
        //
        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(new JLabel("Strength:"), BorderLayout.NORTH);
        this._Delay_Strength = new JSlider();
        this._Delay_Strength.setMaximum(100);
        this._Delay_Strength.setMinimum(0);
        p1.add(this._Echo_Strength, BorderLayout.SOUTH);
        p.add(p1, BorderLayout.NORTH);
        //
        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(new JLabel("Time (ms):"), BorderLayout.NORTH);
        this._Delay_Time = new JSlider();
        this._Delay_Time.setMaximum(50);
        this._Delay_Time.setMinimum(0);
        p2.add(this._Delay_Time, BorderLayout.SOUTH);
        p.add(p2, BorderLayout.SOUTH);
        return p;
    }
    
    private Effect _getCurrentEffect() {
        return Connection.getInstance().getEffect(this._Channel, this._Effect);
    }

    private String[] _getEffects() {
    	//All available effects
        String[] s = {"Passive", "Distortion", "Chorus", "Echo", "Delay", "Low Pass Filter", "High Pass Filter"};
        return s;
    }

}
