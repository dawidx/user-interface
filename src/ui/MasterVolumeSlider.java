package ui;

import javax.swing.*;
import javax.swing.event.*;

/**
 * @description Master Volume slider with ChangeListener added so that when user adjusts the volume there is no need to press any buttons
 * @author Dawid Ochal
 */
public class MasterVolumeSlider extends JSlider implements ChangeListener {
    
    public MasterVolumeSlider() {
        super();
        this.setMaximum(20);
        this.setMinimum(0);
        this.setMajorTickSpacing(1);
        this.addChangeListener(this);
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        engine.Connection.getInstance().setVolume(this.getValue());
    }
    
}
