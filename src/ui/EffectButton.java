/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author Dawid
 */
public class EffectButton extends JButton implements ActionListener {
    
    private int _Channel, _Effect;
    
    public EffectButton(Integer Channel, Integer Effect) {
        super("Ch: " + Channel.toString() + " No: " + Effect.toString());
        this._Channel = Channel;
        this._Effect = Effect;
        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        PropertiesWindow w = new PropertiesWindow(this._Channel, this._Effect);
        w.setVisible(true);
    }
    
}
